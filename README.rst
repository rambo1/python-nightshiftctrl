==============
nightshiftctrl
==============

Control LightModes NightShift LED controller over BLE

See local devel for installing full devel deps with Poetry

Start the service that keeps the BLE connections alive and passes your commands on::

    nightshift_service --defaultconfig >config.toml && nightshift_service -v config.toml

Use the control interface::

    nightshift --help
    nightshift brightness 15 # (low from app, or 80 for high on app but valid values go up to 99)
    nightshift hsv 230 100 100  # Set HSV main color for patterns, mapped from degrees and parcents to the nighshift internal values
    nightshift solid 80 100 100 # Set solid color mode and color in HSV
    nightshift audio # audio FFT mode
    nightshift pattern 8 # select pattern (1-19 currently)

If you're running the service only in TCP socket or different path remember to set the socket uri via -s

Experimenting
-------------

Subscribe to notifications topic to see responses from the device::

    testsubscriber -s ipc:///tmp/nightshiftctrl_pub.sock -t notifications

Sending arbitrary commands::

    nightshift send "PATT 1007"
    nightshift send "OPTS 050099"

See https://gitlab.com/rambo1/nightshift_bt_re for notes on reverse engineering the protocol and captures
on the BLE traffic etc.


Docker
------

For more controlled deployments and to get rid of "works on my computer" -syndrome, we always
make sure our software works under docker.

It's also a quick way to get started with a standard development environment.

SSH agent forwarding
^^^^^^^^^^^^^^^^^^^^

We need buildkit_::

    export DOCKER_BUILDKIT=1

.. _buildkit: https://docs.docker.com/develop/develop-images/build_enhancements/

And also the exact way for forwarding agent to running instance is different on OSX::

    export DOCKER_SSHAGENT="-v /run/host-services/ssh-auth.sock:/run/host-services/ssh-auth.sock -e SSH_AUTH_SOCK=/run/host-services/ssh-auth.sock"

and Linux::

    export DOCKER_SSHAGENT="-v $SSH_AUTH_SOCK:$SSH_AUTH_SOCK -e SSH_AUTH_SOCK"

Creating a development container
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Build image, create container and start it (if you hate privileged map the BLE device somehow)::

    docker build --progress plain --ssh default --target devel_shell -t nightshiftctrl:devel_shell .
    docker create --privileged --name nightshiftctrl_devel -p 59137:59137 -p 59138:59138 -v `pwd`":/app" -it -v /tmp:/tmp `echo $DOCKER_SSHAGENT` nightshiftctrl:devel_shell
    docker start -i nightshiftctrl_devel

pre-commit considerations
^^^^^^^^^^^^^^^^^^^^^^^^^

If working in Docker instead of native env you need to run the pre-commit checks in docker too::

    docker exec -i nightshiftctrl_devel /bin/bash -c "pre-commit install"
    docker exec -i nightshiftctrl_devel /bin/bash -c "pre-commit run --all-files"

You need to have the container running, see above. Or alternatively use the docker run syntax but using
the running container is faster::

    docker run -it --rm -v `pwd`":/app" nightshiftctrl:devel_shell -c "pre-commit run --all-files"

Test suite
^^^^^^^^^^

You can use the devel shell to run py.test when doing development, for CI use
the "tox" target in the Dockerfile::

    docker build --progress plain --ssh default --target tox -t nightshiftctrl:tox .
    docker run -it --rm -v `pwd`":/app" `echo $DOCKER_SSHAGENT` nightshiftctrl:tox

Production docker
^^^^^^^^^^^^^^^^^

There's a "production" target as well for running the application (if you hate privileged map the BLE device somehow),
remember to change that architecture tag to arm64 if building on ARM::

    docker build --progress plain --ssh default --target production -t nightshiftctrl:latest .
    docker run --privileged -it --name nightshiftctrl -v myconfig.toml:/app/config.toml -p 59137:59137 -p 59138:59138 -v /tmp:/tmp `echo $DOCKER_SSHAGENT` nightshiftctrl:amd64-latest


Local Development
-----------------

TLDR:

- Create and activate a Python 3.8 virtualenv (assuming virtualenvwrapper)::

    mkvirtualenv -p `which python3.8` my_virtualenv

- change to a branch::

    git checkout -b my_branch

- install Poetry: https://python-poetry.org/docs/#installation
- Install project deps and pre-commit hooks::

    poetry install
    pre-commit install
    pre-commit run --all-files

- Ready to go, try the following::

    nightshiftctrl --defaultconfig >config.toml
    nightshiftctrl -vv config.toml

Remember to activate your virtualenv whenever working on the repo, this is needed
because pylint and mypy pre-commit hooks use the "system" python for now (because reasons).

Running "pre-commit run --all-files" and "py.test -v" regularly during development and
especially before committing will save you some headache.
