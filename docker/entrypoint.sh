#!/bin/bash -l
set -e
if [ "$#" -eq 0 ]; then
  exec nightshift_service -vv docker_config.toml
else
  exec "$@"
fi
