"""Default configuration file contents"""

# Remember to add tests for keys into test_nightshiftctrl.py
DEFAULT_CONFIG_STR = """
[zmq]
pub_sockets = ["ipc:///tmp/nightshiftctrl_pub.sock", "tcp://*:59137"]
rep_sockets = ["ipc:///tmp/nightshiftctrl_rep.sock", "tcp://*:59138"]

[nightshift]
devices = []  # Leave empty to scan all, specify to limit
scan_interval = 1.5  # how often to look for new devices
scan_timeout = 2.0  # timeout for device scans
cmd_timeout = 0.5  # Timeout for device commands
keepalive = 10.0  # how often to send PWR for keepalive

""".lstrip()
