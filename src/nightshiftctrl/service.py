"""Main classes for nightshiftctrl"""
from __future__ import annotations
from typing import Dict, Callable, Optional, Tuple, cast
import asyncio
from dataclasses import dataclass, field
import logging
import functools


from datastreamservicelib.service import SimpleService
from datastreamservicelib.reqrep import REPMixin
from datastreamcorelib.datamessage import PubSubDataMessage
from bleak import BleakScanner, BleakClient  # type: ignore
from bleak.backends.client import BaseBleakClient  # type: ignore
from bleak.backends.device import BLEDevice  # type: ignore
from bleak.exc import BleakError  # type: ignore

LOGGER = logging.getLogger(__name__)
NOTIFY_UUID = "beb5483e-36e1-4688-b7f5-ea07361b26a8"
WRITE_SERVICE_UUID = "4fafc201-1fb5-459e-8fcc-c5c9c331914b"
WRITE_CHAR_UUID = "beb5483e-36e1-4688-b7f5-ea07361b26a8"
SCAN_SERVICE_UUIDS = [WRITE_SERVICE_UUID]
MAX_PATTERN_NO = 19


@dataclass
class NightShiftService(REPMixin, SimpleService):
    """Main class for nightshiftctrl"""

    _devices: Dict[str, BLEDevice] = field(default_factory=dict, init=False)
    _clients: Dict[str, BaseBleakClient] = field(default_factory=dict, init=False)

    async def teardown(self) -> None:
        """Called once by the run method before exiting"""
        await self.exit_clients()
        await super().teardown()

    async def _send_client(self, client: BaseBleakClient, cmd: str, *, from_exit: bool = False) -> bool:
        """Send a command to given client instance"""
        # The sniffed writes seem to be with a response, I guess they come to the notifications then
        _cnf = self.config["nightshift"]
        LOGGER.debug("Sending '{}' to {}".format(cmd, client.address))
        try:
            # reminder: Do not add crlf
            await client.write_gatt_char(WRITE_CHAR_UUID, cmd.encode("ASCII"), response=True)
            return True
        except (BleakError, KeyError) as exc:
            LOGGER.error("Write failed: {}".format(exc))
            if from_exit:
                return False
            await self.exit_client(client, hard=True)
            return False

    async def send_all(self, cmd: str) -> bool:
        """Send command to all devices"""
        if not self._clients:
            LOGGER.warning("No connected devices")
            return False
        for client in list(self._clients.values()):
            await self._send_client(client, cmd)
        return True

    async def send(self, cmd: str, addr: Optional[str] = None) -> bool:
        """Send to client in given address, returns false if address could not be matched and if address
        is falsy will fall back to send_all"""
        if not addr:
            return await self.send_all(cmd)
        addrl = addr.lower()
        if addrl not in self._clients:
            LOGGER.warning("No device {}".format(addrl))
            return False
        client = self._clients[addrl]
        await self._send_client(client, cmd)
        return True

    async def set_brightness(self, brightness: int, addr: Optional[str] = None) -> None:
        """Set the brightness"""
        if brightness < 1 or brightness > 100:
            raise ValueError("Brightness must be between 1-100")
        await self.send(f"BRIT {brightness:03d}", addr)

    async def set_audio(self, addr: Optional[str] = None) -> None:
        """Set to audio sync"""
        await self.send("PATT 5", addr)

    async def set_pattern(self, pattern_no: int, addr: Optional[str] = None) -> None:
        """Choose pattern nr"""
        if pattern_no < 1 or pattern_no > MAX_PATTERN_NO:
            raise ValueError(f"pattern_no must be between 0-{MAX_PATTERN_NO}")
        pval = 1000 + pattern_no
        await self.send(f"PATT {pval:d}", addr)

    async def set_solid(self, hsv: Optional[Tuple[int, int, int]] = None, addr: Optional[str] = None) -> None:
        """Set to solid color mode, optionally set the color too"""
        await self.send("PATT 0", addr)
        if hsv:
            await self.set_color(hsv[0], hsv[1], hsv[2], addr=addr)

    async def set_color(self, hue: int, saturation: int, value: int, addr: Optional[str] = None) -> None:
        """Set HSV color"""
        saturation = int(saturation)
        value = int(value)
        hue = int(hue)
        if saturation < 0 or saturation > 100:
            raise ValueError("Saturation must be between 0-100")
        if value < 0 or value > 100:
            raise ValueError("Value must be between 0-100")
        if hue < 0 or hue > 360:
            raise ValueError("Hue must be between 0-360")
        nshue = round((hue / 360) * 24)  # hue 0-24
        nssat = round((saturation / 100) * 90)  # saturation 0-90
        nsval = round((value / 100) * 99)  # value 0-99
        cmd = f"OPTS {nshue:02d}{nssat:02d}{nsval:02d}"
        await self.send(cmd, addr)

    async def exit_client(self, client: BaseBleakClient, *, hard: bool = False) -> None:
        """exit a given client"""
        if not hard:
            await client.disconnect()
        key = client.address.lower()
        del self._devices[key]
        del self._clients[key]
        await self.stop_named_task_graceful(f"keepalive_{client.address}")

    async def exit_clients(self) -> None:
        """Exit all client contexts"""
        for client in list(self._clients.values()):
            await self.exit_client(client)
        self._clients = {}

    async def keepalive(self, client: BaseBleakClient) -> None:
        """Keep the connection alive"""
        cnf = self.config["nightshift"]
        key = client.address.lower()
        while key in self._clients:
            try:
                LOGGER.debug("Keeping {} alive".format(client.address))
                await self._send_client(client, "POWR")
                await asyncio.sleep(cnf["keepalive"])
            except asyncio.CancelledError:
                return

    async def publish_task(self, msg: PubSubDataMessage) -> None:
        """publish given msg, needed to wrap non-async callbacks"""
        await self.psmgr.publish_async(msg)

    async def connect_device(self, dev: BLEDevice) -> None:
        """Setup a client context for given device"""
        if dev in self._devices:
            LOGGER.warning("{} already tracked".format(dev))
            return
        LOGGER.info("connecting {}".format(dev))
        addr = dev.address.lower()
        self._devices[addr] = dev
        self._clients[addr] = BleakClient(self._devices[addr])
        await self._clients[addr].connect()

        def handle_notify(dev: BLEDevice, hdl: int, data: bytearray) -> None:
            """Handle notifications"""
            nonlocal self
            LOGGER.debug("received on {} handle {}: {}".format(dev, hdl, repr(data)))
            msg = PubSubDataMessage(f"notifications_{dev.address}")
            msg.data.update({"addr": dev.address, "hdl": hdl, "data": data})
            self.create_task(self.publish_task(msg))
            # TODO parse notifacations for responses (like f we query battery level)

        await self._clients[addr].start_notify(
            NOTIFY_UUID, cast(Callable[[int, bytes], None], functools.partial(handle_notify, self._devices[addr]))
        )
        await self._send_client(self._clients[addr], "POWR")
        await self._send_client(self._clients[addr], "VERS")
        self.create_task(self.keepalive(self._clients[addr]), name=f"keepalive_{self._clients[addr].address}")

    async def scan_devices(self) -> None:
        """Scan for devices"""
        cnf = self.config["nightshift"]
        devices_lower = [addr.lower() for addr in cnf["devices"]]
        devices = await BleakScanner.discover(service_uuids=SCAN_SERVICE_UUIDS, timeout=cnf["scan_timeout"])
        for dev in devices:
            addr = dev.address.lower()
            if devices_lower and addr not in devices_lower:
                LOGGER.debug("Skipping {} because it's not in {}".format(dev, devices_lower))
                continue
            if addr in self._devices:
                # Already tracked
                continue
            await self.connect_device(dev)

    async def scan_task(self) -> None:
        """Pediodically scan for devices"""
        cnf = self.config["nightshift"]
        while True:
            try:
                LOGGER.debug("Scanning for devices")
                await self.scan_devices()
                await asyncio.sleep(cnf["scan_interval"])
            except asyncio.CancelledError:
                return

    async def async_reload(self) -> None:
        """Async stuff to done on every reload"""
        await self.exit_clients()
        if "devicescan" in self._tasks:
            await self.stop_named_task_graceful("devicescan")
        self.create_task(self.scan_task(), name="devicescan")

    def reload(self) -> None:
        """Load configs, restart sockets"""
        super().reload()
        self.create_task(self.async_reload(), name="RELOAD")
