"""CLI entrypoints for nightshiftctrl"""
from typing import Any, cast
import asyncio
import sys
import logging
from pathlib import Path

import click
from datastreamcorelib.logging import init_logging
from datastreamcorelib.datamessage import PubSubDataMessage
from datastreamcorelib.abstract import ZMQSocketType, ZMQSocketDescription
from datastreamservicelib.zmqwrappers import Socket, SocketHandler

from nightshiftctrl.defaultconfig import DEFAULT_CONFIG_STR
from nightshiftctrl import __version__
from nightshiftctrl.service import NightShiftService


LOGGER = logging.getLogger(__name__)


def dump_default_config(ctx: Any, param: Any, value: bool) -> None:  # pylint: disable=W0613
    """Print the default config and exit"""
    if not value:
        return
    click.echo(DEFAULT_CONFIG_STR)
    if ctx:
        ctx.exit()


@click.command()
@click.version_option(version=__version__)
@click.option("-l", "--loglevel", help="Python log level, 10=DEBUG, 20=INFO, 30=WARNING, 40=CRITICAL", default=30)
@click.option("-v", "--verbose", count=True, help="Shorthand for info/debug loglevel (-v/-vv)")
@click.option(
    "--defaultconfig",
    is_flag=True,
    callback=dump_default_config,
    expose_value=False,
    is_eager=True,
    help="Show default config",
)
@click.argument("configfile", type=click.Path(exists=True))
def nightshiftservice_cli(configfile: Path, loglevel: int, verbose: int) -> None:
    """Service to control LightModes NightShift LED controller over BLE. takes commands over ZMQ REP"""
    if verbose == 1:
        loglevel = 20
    if verbose >= 2:
        loglevel = 10
    init_logging(loglevel)
    LOGGER.setLevel(loglevel)

    service_instance = NightShiftService(Path(configfile))

    exitcode = asyncio.get_event_loop().run_until_complete(service_instance.run())
    sys.exit(exitcode)


@click.group()
@click.option("-l", "--loglevel", help="Python log level, 10=DEBUG, 20=INFO, 30=WARNING, 40=CRITICAL", default=30)
@click.option("-v", "--verbose", count=True, help="Shorthand for info/debug loglevel (-v/-vv)")
@click.option("-s", "--socketuri", type=str, default="ipc:///tmp/nightshiftctrl_rep.sock", help="ZMQ socket URI")
@click.pass_context
def ctrlcommonopts(ctx: Any, loglevel: int, verbose: int, socketuri: str) -> None:
    """CLI wrapper for select REP calls in LMServoService"""
    if verbose == 1:
        loglevel = 20
    if verbose >= 2:
        loglevel = 10
    logging.getLogger("").setLevel(loglevel)
    LOGGER.setLevel(loglevel)
    ctx.ensure_object(dict)
    ctx.obj["loop"] = asyncio.get_event_loop()
    ctx.obj["hdlr"] = SocketHandler()
    req_sdesc = ZMQSocketDescription(socketuri, ZMQSocketType.REQ)
    ctx.obj["req_socket"] = cast(Socket, ctx.obj["hdlr"].get_socket(req_sdesc))


async def send_cmd_get_reply(ctx: Any, cmd: PubSubDataMessage) -> PubSubDataMessage:
    """Send a command via REP, return the reply"""
    try:
        await asyncio.wait_for(ctx.obj["req_socket"].send_multipart(cmd.zmq_encode()), timeout=0.1)
        replyparts = await asyncio.wait_for(ctx.obj["req_socket"].recv_multipart(), timeout=0.5)
        return PubSubDataMessage.zmq_decode(replyparts)
    except asyncio.TimeoutError:
        reply = PubSubDataMessage("reply")
        reply.data.update({"failed": True, "reason": "Socket timeout"})
        return reply


@ctrlcommonopts.command()
@click.argument("cmdstr", type=str)
@click.option("-d", "--device", help="Device mac address")
@click.pass_context
def send(ctx: Any, cmdstr: str, device: str) -> None:
    """Send a raw command to given device or all connected"""

    async def call_req(ctx: Any, cmdstr: str, device: str) -> int:
        """Create message, call method, check reply"""
        cmd = PubSubDataMessage(b"command")
        if device:
            cmd.data.update({"command": ["send", device, cmdstr]})
        else:
            cmd.data.update({"command": ["send_all", cmdstr]})
        reply = await send_cmd_get_reply(ctx, cmd)
        if reply.data["failed"]:
            click.echo("Failed: {}".format(reply.data["reason"]))
            return 1
        return 0

    sys.exit(ctx.obj["loop"].run_until_complete(call_req(ctx, cmdstr, device)))


@ctrlcommonopts.command()
@click.argument("hue", type=int)
@click.argument("sat", type=int)
@click.argument("val", type=int)
@click.option("-d", "--device", help="Device mac address")
@click.pass_context
def hsv(ctx: Any, device: str, hue: int, sat: int, val: int) -> None:
    """Send HSV color"""

    async def call_req() -> int:
        """Create message, call method, check reply"""
        nonlocal hue, sat, val, ctx, device
        cmd = PubSubDataMessage(b"command")
        cmd.data.update({"command": ["set_color", hue, sat, val]})
        if device:
            cmd.data["command"].append(device)
        reply = await send_cmd_get_reply(ctx, cmd)
        if reply.data["failed"]:
            click.echo("Failed: {}".format(reply.data["reason"]))
            return 1
        return 0

    sys.exit(ctx.obj["loop"].run_until_complete(call_req()))


@ctrlcommonopts.command()
@click.argument("hue", type=int)
@click.argument("sat", type=int)
@click.argument("val", type=int)
@click.option("-d", "--device", help="Device mac address")
@click.pass_context
def solid(ctx: Any, device: str, hue: int, sat: int, val: int) -> None:
    """Set solid HSV color"""

    async def call_req() -> int:
        """Create message, call method, check reply"""
        nonlocal hue, sat, val, ctx, device
        cmd = PubSubDataMessage(b"command")
        cmd.data.update({"command": ["set_solid", [hue, sat, val]]})
        if device:
            cmd.data["command"].append(device)
        reply = await send_cmd_get_reply(ctx, cmd)
        if reply.data["failed"]:
            click.echo("Failed: {}".format(reply.data["reason"]))
            return 1
        return 0

    sys.exit(ctx.obj["loop"].run_until_complete(call_req()))


@ctrlcommonopts.command()
@click.argument("patno", type=int)
@click.option("-d", "--device", help="Device mac address")
@click.pass_context
def pattern(ctx: Any, device: str, patno: int) -> None:
    """Set pattern"""

    async def call_req() -> int:
        """Create message, call method, check reply"""
        nonlocal patno, ctx, device
        cmd = PubSubDataMessage(b"command")
        cmd.data.update({"command": ["set_pattern", int(patno)]})
        if device:
            cmd.data["command"].append(device)
        reply = await send_cmd_get_reply(ctx, cmd)
        if reply.data["failed"]:
            click.echo("Failed: {}".format(reply.data["reason"]))
            return 1
        return 0

    sys.exit(ctx.obj["loop"].run_until_complete(call_req()))


@ctrlcommonopts.command()
@click.argument("brightval", type=int)
@click.option("-d", "--device", help="Device mac address")
@click.pass_context
def brightness(ctx: Any, device: str, brightval: int) -> None:
    """Set brightness"""

    async def call_req() -> int:
        """Create message, call method, check reply"""
        nonlocal brightval, ctx, device
        cmd = PubSubDataMessage(b"command")
        cmd.data.update({"command": ["set_brightness", int(brightval)]})
        if device:
            cmd.data["command"].append(device)
        reply = await send_cmd_get_reply(ctx, cmd)
        if reply.data["failed"]:
            click.echo("Failed: {}".format(reply.data["reason"]))
            return 1
        return 0

    sys.exit(ctx.obj["loop"].run_until_complete(call_req()))


@ctrlcommonopts.command()
@click.option("-d", "--device", help="Device mac address")
@click.pass_context
def audio(ctx: Any, device: str) -> None:
    """Set to audio sync"""

    async def call_req() -> int:
        """Create message, call method, check reply"""
        nonlocal ctx, device
        cmd = PubSubDataMessage(b"command")
        cmd.data.update({"command": ["set_audio"]})
        if device:
            cmd.data["command"].append(device)
        reply = await send_cmd_get_reply(ctx, cmd)
        if reply.data["failed"]:
            click.echo("Failed: {}".format(reply.data["reason"]))
            return 1
        return 0

    sys.exit(ctx.obj["loop"].run_until_complete(call_req()))


def nightshiftservice_ctrl() -> None:
    """Control the service via REP socket"""
    ctrlcommonopts()  # pylint: disable=E1120
